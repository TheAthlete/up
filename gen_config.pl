#!/usr/bin/env perl
use strict;
use warnings;
use feature 'say';
use Mojo::Template;
use DDP;

use FindBin qw/$Bin/;

my $root = shift;

my $ubic_app = "$Bin/etc/ubic/app";
my $ubic_tmpl = $ubic_app.".in";
my $mt = Mojo::Template->new(vars => 1);
my $output = $mt->render_file($ubic_tmpl, {path => {root => $root}});
# say $output;

open my $fh, '>', $ubic_app;
print $fh $output;
close $fh;
