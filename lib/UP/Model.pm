use utf8;
package UP::Model;

# Created by DBIx::Class::Schema::Loader
# DO NOT MODIFY THE FIRST PART OF THIS FILE

use strict;
use warnings;

use base 'DBIx::Class::Schema';

__PACKAGE__->load_namespaces;


# Created by DBIx::Class::Schema::Loader v0.07042 @ 2015-11-01 14:39:18
# DO NOT MODIFY THIS OR ANYTHING ABOVE! md5sum:3P2Y/j0CqX4oKGWOjXBUjg


# You can replace this text with custom code or comments, and it will be preserved on regeneration
1;
