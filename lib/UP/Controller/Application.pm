package UP::Controller::Application;
use Mojo::Base 'Mojolicious::Controller';

use Data::Printer;
use UP::Model;

sub list {
  my $c = shift;

}

sub items {
  my $c = shift;

}

sub admin {
  my $c = shift;

  return $c->reply->not_found() unless $c->stash('user');

  return $c->render('application/admin');
}

1;
