package UP::Controller::User;
use Mojo::Base 'Mojolicious::Controller';

use Data::Printer;
use UP::Model;

sub index {
  my $c = shift;

  my $usr_rs = $c->up_dbh->resultset('Usr');
  
  # TODO: написать скрипт и запускать в кроне, например, раз в сутки
  my $delete_users = $usr_rs->search({
    mtime => { '<' => \q|now() - interval '3 month'|},
  })->delete;

  $c->stash(msg => 'Welcome');

  $c->render(template => 'user/index');
}

sub registration {
  my $c = shift;

  if ($c->req->json) {
    $c->param($_ => $c->req->json->{$_}) for keys %{$c->req->json};
  }

  my $email = $c->param('email') || '';
  my $pass  = $c->param('pass') || '';

  # return $c->render('user/registration') unless $c->req->method() eq 'POST';
  return $c->respond_to(
    json => sub { $c->render( json => {} );        },
    html => sub { $c->render('user/registration'); },
  ) unless $c->req->method() eq 'POST';

  unless ($email && $pass) {
    my $err_text = 'Поле не может быть пустым';
    my $error = { 
      error => {
        $email ? () : (email => 0), 
        $pass  ? () : (pass => 0),
        text => $err_text,
      },
    };

    return $c->render(json => $error); 
    # $c->respond_to(
    #   json => sub { $c->render(json => $error); },
    #   html => sub { $c->flash( %$error ); $c->redirect_to('registration'); },
    # );

  }

  $c->redirect_to( $c->url_for('index')->query(ok => 1) ) if $c->stash('user');

  # TODO: получение хэша таким образом не работает в тестах Test::Mojo (не все поля отдаются, возможно из за того, что AJAX-запросы)
  # либо просто не сохраняется в req
  # my $data = $c->req->params->to_hash; 
  my $data  = { map { $_ => $c->param($_) } qw(email first_name last_name pass) };
  # $c->app->log->debug('data: '.p($data));

  my $rs  = $c->up_dbh->resultset('Usr');
  
  ($data->{pass}, $data->{salt}) = $rs->result_class()->gen_pass($pass, $pass);
  # $c->app->log->debug('data: '.p($data));

  my $new;
  if (
         $data 
      && $data->{email} 
      && $data->{pass}
      && ! $rs->find({email => $data->{email}}, {key => "usr_email_idx"})
    ) {
      $new = $rs->create($data);
  }
  # $c->app->log->debug('new: '.p($new)) if $new;

  my %res;
  if ($new) {
    $c->cookie(auth => 1, {path => '/', expires => time + 3600*24*365});
    $c->stash(user => $new)->session(user_id => $new->id); # Сохраняем в сессию юзера
  } else {
    $res{error} = {email => 1, em => $email, text => 'Пользователь с таким email уже существует'};
    # $c->stash(error => {email => 1, text => 'Пользователь с таким email уже существует'});
    # $c->stash(error => {email => 'exists'});
  }

  # if ($new) {
  #   $c->redirect_to( $c->url_for('index')->query(reg_ok => 1) );
  # } else {
  #   $c->render(json => { error => $res{error} });
  # }
  
  return $c->render(json => {
      $new ? (ok => 1, redirect_to => $c->url_for('index')->query(reg_ok => 1) )
           : (error => $res{error})
    });

  # $c->flash(%res);
  # my $index_url = $c->url_for('index')->query(reg_ok => 1);
  # return $c->respond_to(
  #   json => sub {
  #     $c->render( json => {
  #         $new ? (ok => 1, redirect_to => $index_url)
  #         # $new ? (ok => 1, redirect_to => $c->url_for('index')->query(reg_ok => 1) )
  #              : (error => $res{error})
  #              # : (error => $c->stash('error'))
  #       });
  #   },
  #   html => sub { 
  #       $c->redirect_to( $new ? $index_url : 'registration');
  #         # $new ? $c->url_for('index')->query(reg_ok => 1) 
  #   },
  # );

}

1;
