package UP::Controller::Authentication;
use Mojo::Base 'Mojolicious::Controller';

use Data::Printer;
use UP::Model;

# страница с формой для авторизации (логина) - обычно GET-запрос
sub signin {
  my $c = shift;

  $c->redirect_to('index') if $c->stash('user');

  $c->render('authentication/signin');
}

sub logout {
  my $c = shift;

  delete $c->stash->{user};
  delete $c->session->{user_id};

  $c->redirect_to('index');
}

# POST-запрос, обработка логина пользователя со страницы signin
sub login {
  my $c = shift;

  my $user = $c->stash('user');

  if ($user) {
    $c->cookie(auth => 1, {path => '/', expires => time + 3600*24*365});
    $c->stash(user => $user)->session(user_id => $user->id() );
    # $c->redirect_to( $c->url_for('admin')->query(login => 1) );
    # return $c->render(json => {email => $user->email, login => 1});
    return $c->redirect_to( $c->url_for('admin')->query(login => 1) );
  }

  if ($c->req->json) {
    $c->param($_ => $c->req->json->{$_}) for keys %{$c->req->json};
  }

  my $params  = { map { $_ => scalar $c->param($_) } qw(email pass) };
  # $c->app->log->debug('params: '.p($params));

  return $c->render(json => {
      error => { text => "Email или пароль не заданы" }
    }) unless $params->{email} && $params->{pass};

  # my $rs = $c->up_dbh->resultset('Usr');
  # my $usr_in_db = $rs->find({email => $params->{email}}, {key => "usr_email_idx"})
  # my $usr_in_db = $c->up_dbh->resultset('Usr')->find({email => $params->{email}}, {key => "usr_email_idx"})
  return $c->render(json => {
      error => {
        email => 0,
        login => 0,
        text => 'Данный пользователь не зарегистрирован', 
      },
    }) unless my $usr_in_db = $c->up_dbh->resultset('Usr')->find({email => $params->{email}}, {key => "usr_email_idx"});
  # return $c->render(json => {email => $params->{email}, error => 'the user is not registered', login => 0}) unless $usr_in_db;

  my $ok = 0;
  if ($usr_in_db && $usr_in_db->check_pass($params->{pass})) {
    $c->cookie(auth => 1, {path => '/', expires => time + 3600*24*365});
    $c->stash(user => $usr_in_db)->session(user_id => $usr_in_db->id() );
    $usr_in_db->update({mtime => \'NOW()'});
    $ok = 1;
  }

  return $c->render(json => {
      ok => $ok, 
      redirect_to => $c->url_for('admin')->query(login => $ok) 
    });
  # TODO: реализовать редирект вместо возврата json
  # $c->redirect_to( $c->url_for($route_name)->query(login => $ok) );
  # $c->redirect_to( $c->url_for('admin')->query(login => $ok) );
  # return $c->render(json => {email => $usr_in_db->email, login => $ok});
}

1;
