use utf8;
package UP::Model::Result::Usr;

use Mojo::Util 'sha1_sum';

=head1 NAME

UP::Model::Result::Usr

=cut

use strict;
use warnings;

use base 'DBIx::Class::Core';

=head1 COMPONENTS LOADED

=over 4

=item * L<DBIx::Class::InflateColumn::DateTime>

=back

=cut

__PACKAGE__->load_components("InflateColumn::DateTime");

=head1 TABLE: C<usr>

=cut

__PACKAGE__->table("usr");

=head1 ACCESSORS

=head2 id

  data_type: 'bigserial'
  is_auto_increment: 1
  is_nullable: 0
  sequence: 'usr_id_seq'

=head2 first_name

  data_type: 'text'
  is_nullable: 1

=head2 last_name

  data_type: 'text'
  is_nullable: 1

=head2 email

  data_type: 'text'
  is_nullable: 0

=head2 salt

  data_type: 'text'
  is_nullable: 0

=head2 pass

  data_type: 'text'
  is_nullable: 0

=head2 ctime

  data_type: 'timestamp with time zone'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=head2 mtime

  data_type: 'timestamp with time zone'
  default_value: current_timestamp
  is_nullable: 0
  original: {default_value => \"now()"}

=cut

__PACKAGE__->add_columns(
  id =>
  {
    data_type         => "bigserial",
    is_auto_increment => 1,
    is_nullable       => 0,
    sequence          => "usr_id_seq",
  },
  first_name  => { data_type => "text",       is_nullable => 1 },
  last_name   => { data_type => "text",       is_nullable => 1 },
  email       => { data_type => "text",       is_nullable => 0 },
  salt        => { data_type => "text",       is_nullable => 0 },
  pass        => { data_type => "text",       is_nullable => 0 },
  ctime       =>
  {
    data_type     => "timestamp with time zone",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
  mtime       =>
  {
    data_type     => "timestamp with time zone",
    default_value => \"current_timestamp",
    is_nullable   => 0,
    original      => { default_value => \"now()" },
  },
);

=head1 PRIMARY KEY

=over 4

=item * L</id>

=back

=cut

__PACKAGE__->set_primary_key("id");

=head1 UNIQUE CONSTRAINTS

=head2 C<usr_email_project_idx>

=over 4

=item * L</email>

=item * L</arisen_from_project>

=back

=cut

__PACKAGE__->add_unique_constraint("usr_email_idx", ["email"]);

sub check_pass {
  my ($self, $pass) = (shift, shift || '');

  return $self->pass() eq sha1_sum $pass . $self->salt();
}

sub gen_pass {
    my ($self, $pass, $confirm) = @_;

    return unless 
               $pass && $confirm
            && $pass eq $confirm
            && length $pass >= 6
            && $pass =~ /^[0-9a-zA-Z]+$/;

    my $salt  = join '', 
                map  chr 33 + rand 95,
                1 .. 10;

    return (sha1_sum( $pass . $salt ), $salt);
}

1;
