package UP;
use Mojo::Base 'Mojolicious';

use UP::Model;

use Data::Printer;

# This method will run once at server start
sub startup {
  my $self = shift;

  my $home = $self->home();

  my $conf = $self->plugin(Config => {file => $home->rel_file('etc/up.conf')});

  $self->mode($conf->{mode});
  $self->secrets([$conf->{secret}]);
  $self->sessions->cookie_name('up');
  $self->sessions->default_expiration($conf->{expires});

  # $self->app->log->debug('self: '.p($self->sessions));

  $self->config(hypnotoad => {
      listen => ['http://*:8008'],
    });

  $self->helper(up_dbh => sub {
      my $up_srv = $conf->{db_servers_up}[0];
      my $up_dbh = UP::Model->connect($up_srv->{dsn}, $up_srv->{username}, $up_srv->{password}, {AutoCommit => 1});
      return $up_dbh;
    });

  $self->hook(
    before_dispatch => sub {
      my $c = shift;

      my $user_id = $c->session('user_id');

      # достаем текущего пользователя
      my $user = $user_id && $c->up_dbh->resultset('Usr')->find($user_id);

      if ($user) {
        $c->stash(user => $user);
      }
    });

  # Router
  my $r = $self->routes;

  $r->get('/signin')->name('signin')->to('authentication#signin');
  $r->get('/logout')->name('logout')->to('authentication#logout');
  $r->post('/login')->name('login') ->to('authentication#login');

  $r->get('/')->name('index')->to('user#index');
  $r->any('/registration')->name('registration')->to('user#registration');

  $r->get('/admin')->name('admin')->to('Application#admin');
  $r->get('/list')->name('list')->to('Application#list');
  $r->get('/items')->name('items')->to('Application#items');

}

1;
