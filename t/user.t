use Mojo::Base -strict;

use Test::More;
use Test::Mojo;
use Data::Printer;
use UP::Model;
use FindBin;

my $t = Test::Mojo->new('UP');

# конфиг
my $conf = do "$FindBin::Bin/../etc/up.conf";

# коннект к базе
# TODO: реализовать с помощью Test::DBIx::Class
my $up_srv = $conf->{db_servers_up}[0];
my $up_dbh = UP::Model->connect($up_srv->{dsn}, $up_srv->{username}, $up_srv->{password}, {AutoCommit => 1});

$ENV{'DISABLE_DBIC_TRACE'} = 1;

my $rs = $up_dbh->resultset('Usr');
my $data = {email => 'test@email.com', first_name => 'Vladimir', last_name => 'Sokolov', pass => '1234567'};

# curl -c newcookies.txt -i -s -X POST 127.0.0.1:8008/registration -d '{"email": "sokolov@email.com", "pass": "1234567", "first_name": "Vladimir", "last_name": "Sokolov"}'
# curl -b newcookies.txt -c newcookies.txt -i -s -X POST 127.0.0.1:8008/login -d '{"email": "sokolov@email.com", "pass": "1234567"}'
# curl -b newcookies.txt -c newcookies.txt -i -s -X GET 127.0.0.1:8008/admin
# curl -b newcookies.txt -c newcookies.txt -i -s -X GET 127.0.0.1:8008/
# curl -b newcookies.txt -c newcookies.txt -i -s -X GET 127.0.0.1:8008/logout
# curl -b newcookies.txt -c newcookies.txt -i -s -X GET 127.0.0.1:8008/
subtest 'user registration, login, logout' => sub {
  my $stash;
  $t->app->hook(after_dispatch => sub { my $self = shift; $stash = $self->stash; });

  # 1. Регистрируем юзера
  #----------------------
  $t->post_ok('/registration' => json => $data)
    ->status_is(200)
    ->json_is('/ok' => 1)
    ->json_is('/redirect_to' => '/?reg_ok=1');
    # ->header_is(location => '/?reg_ok=1');
    # ->header_is(location => Mojo::URL->new("/?reg_ok=1")->to_string);
    
    # warn 'post: '.p($t->tx->res->body);
    # warn 'cookies: '.p($t->tx->res->cookies);
    # warn 'cookies: '.p($t->tx->res->cookies->[0]->to_string);
  
  is $stash->{user}->email,      $data->{email},      'email is set in stash';
  is $stash->{user}->first_name, $data->{first_name}, 'first_name is set in stash';
  is $stash->{user}->last_name, $data->{last_name}, 'first_name is set in stash';
  ok $stash->{user}->check_pass($data->{pass}),       'password is checked';
  #----------------------

  $t->post_ok('/login' => json => $data)
    ->header_is(location => '/admin?login=1');

  $t->get_ok('/admin')->status_is(200);
  $t->get_ok('/logout')->header_is(location => '/');
  $t->get_ok('/admin')->status_is(404);

  $t->post_ok('/login' => json => $data)
    ->status_is(200)
    ->json_is('/ok' => 1)
    ->json_is('/redirect_to' => '/admin?login=1');

  $t->post_ok('/registration' => json => $data)
    ->header_is(location => '/?ok=1');

  $t->get_ok('/admin')->status_is(200);
  $t->get_ok('/logout')->header_is(location => '/');
  $t->get_ok('/admin')->status_is(404);

  $t->get_ok('/signin')->status_is(200);

  $t->post_ok('/login' => json => $data)
    ->status_is(200)
    ->json_is('/ok' => 1)
    ->json_is('/redirect_to' => '/admin?login=1');

  $t->get_ok('/signin')->header_is(location => '/');

  # Удаляем юзера
  if (my $usr_in_db_remove = $rs->find({email => $data->{email}}, {key => 'usr_email_idx'})) {
    $usr_in_db_remove->delete;
  }
};

done_testing();
