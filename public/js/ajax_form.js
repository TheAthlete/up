$(document).ready(function() {

  var tree = $('#result');
  function r(obj) {
    var html = "<ul>";
    if (obj)
      for (var key in obj) {
        if (typeof obj[key] == "object")
          html += '<li>'+key+':'+r(obj[key])+'</li>';
        else if (typeof obj[key] != "function")
          html +='<li>'+key+':'+obj[key]+'</li>';
        else
          html +='<li>'+key+'()</li>';
      }
    html += "</ul>";
    return html;
  } 

  // $('#result').html(r(jQuery));

  var obj = {a: 1, b: 2, c: 3};
  console.log(obj);
  var hasOwn = Object.prototype.hasOwnProperty;

  console.log(hasOwn.call(obj, "a"));
  console.log(hasOwn.call(obj, "d"));

  if (typeof obj.d) {
    console.log("key a in obj exists");
  } else {
    console.log("key a in obj not exists");
  }

  $("#registration_form").submit(function( event ) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission

    $.ajax({
      url : post_url,
      type: request_method,
      data : form_data,
      success: function(response) {
        // console.log(response);
        if (hasOwn.call(response, "ok") && response.ok == 1 && hasOwn.call(response, "redirect_to")) {
          window.location.replace(response.redirect_to); // делаем редирект
        }

        if (hasOwn.call(response, "error")) {
          if (hasOwn.call(response.error, "email")) {
            if (response.error.email == 1) {
              $("#error").text(response.error.text);
            } else if (response.error.email == 0) {
              $("#email_error").text(response.error.text);
            }
            if (response.error.pass == 0) {
              $("#pass_error").text(response.error.text);
            }
          }
        }
      }
    });
  });

  $("#login_form").submit(function( event ) {
    event.preventDefault(); //prevent default action
    var post_url = $(this).attr("action"); //get form action url
    console.log('post_url: ' + post_url);
    var request_method = $(this).attr("method"); //get form GET/POST method
    var form_data = $(this).serialize(); //Encode form elements for submission

    $.ajax({
      url : post_url,
      type: request_method,
      data : form_data,
      success: function(response) {
        console.log(response);
        if (hasOwn.call(response, "ok") && response.ok == 1 && hasOwn.call(response, "redirect_to")) {
          window.location.replace(response.redirect_to); // делаем редирект
        }

        if (hasOwn.call(response, "error")) {
          $("#error").text(response.error.text);
        }
      }
    });
  });
});

// $(document).ready(function() {
// // Attach a submit handler to the form
// $("#registration_form").submit(function( event ) {
//   // // Stop form from submitting normally
//   // event.preventDefault();
//   //
//   // var $form = $( this ),
//   //   email = $form.find("input[name='email']")->val(),
//   //   pass = $form.find("input[name='pass']")->val(),
//   //   first_name = $form.find("input[name='first_name']")->val(),
//   //   last_name = $form.find("input[name='last_name']")->val(),
//   //   url = $form.attr("action");
//   //
//   // var posting = $.post(url,
//   //   {
//   //     'email' : email,
//   //     'pass': pass,
//   //     'first_name': first_name,
//   //     'last_name': last_name
//   //   }
//   // );
//   //
//   // posting.done(function(data) {
//   //   // var content = $(data).find("#content");
//   //   $("#result").empty().append(data);
//   // });
//
//
//   // var post_url = $(this).attr("/registration"); //get form action url
//   // var request_method = $(this).attr("POST"); //get form GET/POST method
//   // var form_data = $(this).serialize(); //Encode form elements for submission
//
//   // $.ajax({
//   //   url : post_url,
//   //   type: request_method,
//   //   data : form_data
//   // }).done(function(response){ //
//   //   alert(response);
//   //   // $("#server-results").html(response);
//   // });
//   
// $("#my_form").submit(function(event) {
//     event.preventDefault(); //prevent default action
//     var post_url = $(this).attr("action"); //get form action url
//     console.log('post_url: ' + post_url);
//     var request_method = $(this).attr("method"); //get form GET/POST method
//     var form_data = $(this).serialize(); //Encode form elements for submission
//    
//     $.ajax({
//         url : post_url,
//         type: request_method,
//         data : form_data
//     }).done(function(response){ //
//         $("#server-results").html(response);
//     });
// });
//
// });
// });
