CREATE DATABASE IF NOT EXISTS `up`
    DEFAULT CHARACTER SET = utf8
    COLLATE = utf8_general_ci;

use `up`;

CREATE TABLE if not exists `usr` (
        `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
        email varchar(50) not null,
        name varchar(100) null,
        salt varchar(10) not null,
        pass varchar(40) not null,
        unique key email (email)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE = utf8_general_ci;
