CREATE USER up_usr WITH PASSWORD 'qwerty';

CREATE DATABASE up
  WITH  OWNER = up_usr
        TEMPLATE = template0
        ENCODING = 'UTF8'
        LC_COLLATE = 'en_US.UTF-8'
        LC_CTYPE = 'en_US.UTF-8';

GRANT ALL PRIVILEGES ON DATABASE up TO up_usr;

\c up;

CREATE SEQUENCE usr_id_seq;

GRANT ALL PRIVILEGES ON SEQUENCE usr_id_seq TO up_usr;

CREATE TABLE usr (
  -- id BIGSERIAL PRIMARY KEY, 
  id BIGINT PRIMARY KEY DEFAULT NEXTVAL('usr_id_seq'), 
  first_name text,
  last_name text,
  email text not null,
  salt text not null,
  pass text not null,
  ctime timestamp with time zone default now() not null,
  mtime timestamp with time zone default now() not null
);

create unique index usr_email_idx on usr(email);

GRANT ALL PRIVILEGES ON usr TO up_usr;
